package ru.ice_aqua.rundrawer.ui;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;
import java.util.Calendar;
import ru.ice_aqua.rundrawer.R;
import ru.ice_aqua.rundrawer.notification.Notific;
import static android.content.Context.ALARM_SERVICE;
import static android.content.Context.MODE_PRIVATE;

public class ProfileFragment extends Fragment {
    private FirebaseUser us = FirebaseAuth.getInstance().getCurrentUser();
    FirebaseStorage storage = FirebaseStorage.getInstance();
    View view;
    ImageView imageView;
    TextView tvName, setPick, tvReset;
    Button b1;
    ImageButton imageButton;
    Shimmer shimmer;
    ShimmerTextView shimmerTextView;
    SharedPreferences sPref;
    final String SAVED_TEXT = "saved_text";


    public ProfileFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        init();
        loadText();
        FloatingActionButton actionButton = (FloatingActionButton)getActivity().findViewById(R.id.fab);
        actionButton.setVisibility(View.INVISIBLE);


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), PhotoActivity.class);
                startActivity(intent);
            }
        });

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar  calendar = Calendar.getInstance();
                final int hour = calendar.get(Calendar.HOUR_OF_DAY);
                final int min = calendar.get(Calendar.MINUTE);

                TimePickerDialog t = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int min) {
                        String currentMin = String.valueOf(min);

                        if (currentMin.length() == 1) {
                                alarm(hour, 0 + min);
                                setPick.setText(hour + ":" + 0 + min);
                        }
                       else {
                           alarm(hour, min);
                           setPick.setText(hour + ":" + min);
                       }
            }
                }, hour, min, true);
                t.show();
            }
        });
        tvReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPick.setText("Не установлено");
                reset();
            }
        });

        return view;
    }

    public void init(){
        shimmerTextView = (ShimmerTextView) view.findViewById(R.id.text_pick);
        imageView = (ImageView) view.findViewById(R.id.profile_iv);
        setPick = (TextView) view.findViewById(R.id.set_pick);
        tvName = (TextView) view.findViewById(R.id.profile_name);
        b1 = (Button) view.findViewById(R.id.upload_photo_profile);
        imageButton = (ImageButton) view.findViewById(R.id.btn_time_pick);
        tvName.setText(us.getEmail());
        shimmer = new Shimmer();
        shimmerTextView.setText("Установлено напоминание :");
        shimmer.start(shimmerTextView);
        tvReset = (TextView) view.findViewById(R.id.reset);


        StorageReference gsReference = storage.getReferenceFromUrl("gs://cloudtask-52c3e.appspot.com/images/" + us.getUid());
        if(gsReference != null)
        Glide.with(getContext()).using(new FirebaseImageLoader()).load(gsReference).into(imageView);
    }

    public void alarm(int hour, int min){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, min);
        calendar.set(Calendar.SECOND, 00);
        Intent intent = new Intent(getContext(), Notific.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    public void reset(){
        Calendar calendar = Calendar.getInstance();
        Intent intent = new Intent(getContext(), Notific.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), 100, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getActivity().getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        alarmManager.cancel(pendingIntent);
    }

    void saveText() {
        sPref = getActivity().getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(SAVED_TEXT, setPick.getText().toString());
        ed.commit();
    }

    void loadText() {
            sPref = getActivity().getPreferences(MODE_PRIVATE);
            String savedText = sPref.getString(SAVED_TEXT, "");
            setPick.setText(savedText);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        saveText();
    }
}
