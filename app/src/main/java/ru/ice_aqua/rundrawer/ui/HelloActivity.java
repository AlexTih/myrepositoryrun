package ru.ice_aqua.rundrawer.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import ru.ice_aqua.rundrawer.auth.LoginActivity;

public class HelloActivity extends AppCompatActivity {
//    private static final int SPLASH_TIME_OUT = 1000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(HelloActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();

   /*    new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(HelloActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);*/
    }
}
