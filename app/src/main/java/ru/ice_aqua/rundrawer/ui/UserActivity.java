package ru.ice_aqua.rundrawer.ui;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import java.util.Calendar;
import de.hdodenhof.circleimageview.CircleImageView;
import ru.ice_aqua.rundrawer.MyFragment2;
import ru.ice_aqua.rundrawer.R;
import ru.ice_aqua.rundrawer.UserList;
import ru.ice_aqua.rundrawer.fragments.BlankFragment;
import ru.ice_aqua.rundrawer.models.UserModel;

public class UserActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "tag";
    MyFragment2 fragment2;
    BlankFragment blankFragment;
    ProfileFragment profileFragment;
    UserList userList;
    EditText etDate, etDistance, etTime;
    ImageButton btnDate;
    Button btnAdd;
    private DatePickerDialog.OnDateSetListener pick;
    private Calendar calendar;
    private int year, mounth, day;
    private AlertDialog dialog1;
    private FirebaseUser us = FirebaseAuth.getInstance().getCurrentUser();
    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    String admin = "blLnhEf0tBW5G2ez7r4sCzQOo0v1";
    String manager = "fNbbabZbt9dCYx96iajCQyxaiE43";
    TextView textView, pickTime;
    StorageReference storageReference;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    CircleImageView imageView;
    NavigationView navigationView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        drawer();
        storageReference = FirebaseStorage.getInstance().getReference();
        fragment2 = new MyFragment2();
        displayView(0);
        pickTime = (TextView) findViewById(R.id.pick);

       FloatingActionButton actionButton = (FloatingActionButton) findViewById(R.id.fab);
        actionButton.setVisibility(View.VISIBLE);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog();
                btnDate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        picker(etDate);
                    }
                });
                btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        writeData();
                        dialog1.dismiss();
                    }
                });
            }
        });



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.user, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void displayView(int position) {
        MyFragment2 fragment = null;
        switch (position) {
            case 0:
                fragment = new MyFragment2();
                break;
            default:
                break;
        }

        if (fragment != null) {
            android.app.FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.conteiner, fragment).commit();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        int id = item.getItemId();

           if (id == R.id.nav_change_password) {
                blankFragment = new BlankFragment();
               transaction.replace(R.id.conteiner, blankFragment);

            } else if (id == R.id.nav_list_of_user) {
               if(us.getUid().equals(admin) || us.getUid().equals(manager)) {
                   userList = new UserList();
                   transaction.replace(R.id.conteiner, userList);
               }
               else {
                   Toast.makeText(getApplicationContext(), R.string.look_user_list, Toast.LENGTH_SHORT).show();
               }
            } else if (id == R.id.nav_home) {
                displayView(0);
            } else if (id == R.id.nav_exit) {
                mAuth.signOut();
                finish();
            }
            else if(id == R.id.nav_profile){
               profileFragment = new ProfileFragment();
               transaction.replace(R.id.conteiner, profileFragment);
            }
                else if (id == R.id.nav_share) {

               } else if (id == R.id.nav_send) {

               }

            transaction.commit();

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }

    public void drawer(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if(us.getUid().equals(manager)) {
            navigationView.getMenu().getItem(1).setVisible(true);
        }
        else {
            navigationView.getMenu().getItem(1).setVisible(false);
        }
        textView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.textViewHeader);
        imageView = (CircleImageView) navigationView.getHeaderView(0).findViewById(R.id.profile_image);
        textView.setText(us.getEmail());
        StorageReference gsReference = storage.getReferenceFromUrl("gs://cloudtask-52c3e.appspot.com/images/" + us.getUid());
        Log.e(TAG, String.valueOf(gsReference));
        Glide.with(getApplicationContext()).using(new FirebaseImageLoader()).load(gsReference).into(imageView);
    }
    public void alertDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(UserActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.fragment_layout, null);
        etDate = (EditText) mView.findViewById(R.id.et_date);
        etDistance = (EditText) mView.findViewById(R.id.et_distance);
        etTime = (EditText) mView.findViewById(R.id.et_time);
        btnAdd = (Button) mView.findViewById(R.id.btn_dialog_add);
        btnDate = (ImageButton) mView.findViewById(R.id.btn_date);
        dialog.setView(mView);
        dialog1 = dialog.create();
        dialog1.show();
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        mounth = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
    }
    public void writeData() {
        UserModel userModel = new UserModel();
        int dis = Integer.parseInt(String.valueOf(etDistance.getText()));
        double t = Double.parseDouble(String.valueOf(etTime.getText()));
        double min = t * 0.0166667;
        double s = dis / min;
        userModel.setDate(etDate.getText().toString());
        etDate.setText("");
        userModel.setDistance(etDistance.getText().toString());
        etDistance.setText("");
        userModel.setId(us.getUid());
        userModel.setTime(etTime.getText().toString());
        etTime.setText("");
        userModel.setName(us.getEmail());
        userModel.setSpeed(s);
        mDatabase.child("message").push().setValue(userModel);
    }
    public void picker(final EditText view) {

        pick = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int mounth, int day) {
                mounth = mounth + 1;
                String date = day + "-" + mounth + "-" + year;
                view.setText(date);
            }
        };
        datePickerDialog();
    }
    public void datePickerDialog(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, android.R.style.Theme_Holo_Dialog, pick, year, mounth, day);
        datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        datePickerDialog.show();
    }
}




