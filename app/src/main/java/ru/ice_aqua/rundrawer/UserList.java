package ru.ice_aqua.rundrawer;

import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import ru.ice_aqua.rundrawer.models.UserModel;

public class UserList extends Fragment {
    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private RecyclerView recyclerView;
    FirebaseRecyclerAdapter<UserModel, UserViewHolder> adapter;
    View view;

    public UserList() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_list, container, false);
        FloatingActionButton actionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        actionButton.setVisibility(View.INVISIBLE);
        recyclerView = (RecyclerView) view.findViewById(R.id.rec_view2);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);

        adapter = new FirebaseRecyclerAdapter<UserModel, UserViewHolder>(
                UserModel.class,
                R.layout.rec2_layout,
                UserViewHolder.class,
                mDatabase.child("users")
        ) {
            @Override
            protected void populateViewHolder(final UserViewHolder viewHolder, final UserModel model, final int position) {
                viewHolder.tvName.setText(model.getName());
                viewHolder.del.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getContext(), R.string.del_user, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        recyclerView.setAdapter(adapter);
        return view;
    }
}