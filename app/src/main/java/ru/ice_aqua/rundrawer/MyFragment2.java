package ru.ice_aqua.rundrawer;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import java.util.Calendar;
import ru.ice_aqua.rundrawer.models.UserModel;

public class MyFragment2 extends Fragment {

    private FirebaseUser us = FirebaseAuth.getInstance().getCurrentUser();
    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private RecyclerView recyclerView;
    String admin = "blLnhEf0tBW5G2ez7r4sCzQOo0v1";
    View view;
    FirebaseRecyclerAdapter<UserModel, UserViewHolder> adapter;
    ImageView imageView;
    TextView tvLogin, textView;
    EditText etDate, etDistance, etTime;
    ImageButton btnDate;
    Button btnAdd;
    private AlertDialog dialog1;
    private Calendar calendar;
    private int year, mounth, day;
    UserModel userModel;
    private DatePickerDialog.OnDateSetListener pick;
    public static final String TAG = "tokenTag";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String recent_token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, recent_token);
        FloatingActionButton actionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        actionButton.setVisibility(View.VISIBLE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment2_layout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView) view.findViewById(R.id.rec_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);


        adapter = new FirebaseRecyclerAdapter<UserModel, UserViewHolder>(
                UserModel.class,
                R.layout.rec_view_layout,
                UserViewHolder.class,
                mDatabase.child("message")
        ) {

            @Override
            protected void populateViewHolder(final UserViewHolder viewHolder, final UserModel model, final int position) {

                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference gsReference = storage.getReferenceFromUrl("gs://cloudtask-52c3e.appspot.com/images/" + model.getId());
                Glide.with(getContext()).using(new FirebaseImageLoader()).load(gsReference).into(viewHolder.imageView);

                viewHolder.tvName.setText(model.getName());
                viewHolder.tvDate.setText(getResources().getString(R.string.date) + ": " + model.getDate());
                viewHolder.tvDistance.setText(getResources().getString(R.string.distance) + ": " + model.getDistance() + " " + getString(R.string.km));
                viewHolder.tvTime.setText(getResources().getString(R.string.time) + ": " + model.getTime() + " " + getString(R.string.min));
                viewHolder.tvSpeed.setText(getResources().getString(R.string.speed) + ": " + (int) model.getSpeed() + " " + getString(R.string.km_hour));

                viewHolder.del.setVisibility(View.INVISIBLE);
                viewHolder.up.setVisibility(View.INVISIBLE);
                viewHolder.share.setVisibility(View.INVISIBLE);


                if (us.getUid().equals(model.getId()) || us.getUid().equals(admin)) {
                    viewHolder.del.setVisibility(View.VISIBLE);
                    viewHolder.del.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            DatabaseReference i = getRef(viewHolder.getAdapterPosition());
                            i.removeValue();
                        }
                    });
                }

                if (us.getUid().equals(model.getId())) {
                    viewHolder.share.setVisibility(View.VISIBLE);
                    viewHolder.share.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            String s = getString(R.string.user) + ": " + model.getName() + ", " + getString(R.string.date) + ": " + model.getDate() + ", " + getString(R.string.distance) + ": " + model.getDistance() + " " + getString(R.string.km) + ", " + getString(R.string.time) + ": " + model.getTime() + " " + getString(R.string.min) + ", " + getString(R.string.speed) + " " + model.getSpeed() + " " + getString(R.string.km_hour);
                            intent.putExtra(Intent.EXTRA_TEXT, s);
                            startActivity(Intent.createChooser(intent, "Share via"));
                        }
                    });
                }

                if (us.getUid().equals(model.getId()) || us.getUid().equals(admin)) {
                    viewHolder.up.setVisibility(View.VISIBLE);
                    viewHolder.up.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog();
                            btnDate.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    picker(etDate);
                                }
                            });
                            btnAdd.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    updateDb();
                                    DatabaseReference i = getRef(viewHolder.getAdapterPosition());
                                    i.setValue(userModel);
                                    dialog1.dismiss();
                                }
                            });
                        }
                    });

                }
            }
        };

        tvLogin = (TextView) view.findViewById(R.id.tv_login);
        tvLogin.setText(us.getEmail());
        recyclerView.setAdapter(adapter);
        imageView = (ImageView) view.findViewById(R.id.iv_strelka);
        imageView.setVisibility(View.INVISIBLE);
        textView = (TextView) view.findViewById(R.id.tv_near_iv);
        textView.setVisibility(View.INVISIBLE);
        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                check();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                check();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });     // Устанавливаем TextView, если список пустой

    }

    public void alertDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);  // Потому что не в активити!!
        View mView = inflater.inflate(R.layout.fragment_layout, null);
        etDate = (EditText) mView.findViewById(R.id.et_date);
        etDistance = (EditText) mView.findViewById(R.id.et_distance);
        etTime = (EditText) mView.findViewById(R.id.et_time);
        btnAdd = (Button) mView.findViewById(R.id.btn_dialog_add);
        btnDate = (ImageButton) mView.findViewById(R.id.btn_date);
        dialog.setView(mView);
        dialog1 = dialog.create();
        dialog1.show();
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        mounth = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
    }

    public void updateDb() {
        userModel = new UserModel();
        int dis = Integer.parseInt(String.valueOf(etDistance.getText()));
        double t = Double.parseDouble(String.valueOf(etTime.getText()));
        double min = t * 0.0166667;
        double s = dis / min;
        userModel.setDate(etDate.getText().toString());
        etDate.setText("");
        userModel.setDistance(etDistance.getText().toString());
        etDistance.setText("");
        userModel.setId(us.getUid());
        userModel.setTime(etTime.getText().toString());
        etTime.setText("");
        userModel.setName(us.getEmail());
        userModel.setSpeed(s);
    }

    public void picker(final EditText view) {

        pick = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int mounth, int day) {
                mounth = mounth + 1;
                String date = day + "-" + mounth + "-" + year;
                view.setText(date);
            }
        };
        datePickerDialog();
    }

    public void datePickerDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), android.R.style.Theme_Holo_Dialog, pick, year, mounth, day);
        datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        datePickerDialog.show();
    }

    public void check() {
        adapter.notifyDataSetChanged();
        if(adapter.getItemCount() == 0) {
            imageView.setVisibility(View.VISIBLE);
            textView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.INVISIBLE);
        } else {
            imageView.setVisibility(View.INVISIBLE);
            textView.setVisibility(View.INVISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
        }

    }

}