package ru.ice_aqua.rundrawer.fragments;

import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import ru.ice_aqua.rundrawer.R;


public class BlankFragment extends Fragment {
    View view;
    private FirebaseUser us = FirebaseAuth.getInstance().getCurrentUser();
    Button btnNewPassword;
    EditText newPassword, repeatPassword;
    String newPasswordProfile, newPass, repeatPass;

    public BlankFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_blank, container, false);
        init();
        FloatingActionButton actionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        actionButton.setVisibility(View.INVISIBLE);

            btnNewPassword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    newPass = newPassword.getText().toString();
                    repeatPass = repeatPassword.getText().toString();
                    if (newPass.equals(repeatPass)) {
                        newPasswordProfile = newPass;
                        Toast.makeText(getContext(), R.string.parol_match, Toast.LENGTH_SHORT).show();
                        updatePassword();
                    } else {
                        Toast.makeText(getContext(), R.string.dont_match, Toast.LENGTH_SHORT).show();
                    }
                }
            });


            return view;
        }

    public void init(){
        btnNewPassword = (Button) view.findViewById(R.id.btn_new_password);
        newPassword = (EditText) view.findViewById(R.id.new_password);
        repeatPassword = (EditText) view.findViewById(R.id.repeat_password);
    }

    public void updatePassword(){
        us.updatePassword(newPasswordProfile).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Toast.makeText(getContext(), R.string.sucsess_password, Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(getContext(), R.string.unsucsess_password, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
