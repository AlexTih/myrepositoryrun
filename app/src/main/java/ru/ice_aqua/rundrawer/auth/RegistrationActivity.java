package ru.ice_aqua.rundrawer.auth;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ru.ice_aqua.rundrawer.R;
import ru.ice_aqua.rundrawer.ui.PromoActivity;

public class RegistrationActivity extends AppCompatActivity {
    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private FirebaseAuth mAuth;
    EditText etEmailReg, etPasswordReg;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        init();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               valid();
            }
        });
    }
    public void init(){
        mAuth = FirebaseAuth.getInstance();
        etEmailReg = (EditText) findViewById(R.id.et_email_reg);
        etPasswordReg = (EditText) findViewById(R.id.et_password_reg);
        button = (Button) findViewById(R.id.btn_reg_reg);
    }

    private void registration(final String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), R.string.reg_succsses, Toast.LENGTH_SHORT).show();
                    mDatabase.child("users").push().child("name").setValue(email);
                    Intent intent = new Intent(RegistrationActivity.this, PromoActivity.class);
                    startActivity(intent);
                } else
                    Toast.makeText(getApplicationContext(), R.string.repeat_pop, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void valid(){
        String validemail = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@" + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+";
        String email = etEmailReg.getText().toString();
        Matcher matcher= Pattern.compile(validemail).matcher(email);
        if (!matcher.matches() || etEmailReg.getText().toString().trim().isEmpty()){
            etEmailReg.setError(getString(R.string.error_email));
        }

        if(etPasswordReg.getText().toString().trim().length() < 6 || etPasswordReg.getText().toString().trim().isEmpty()){
            etPasswordReg.setError(getString(R.string.length_password));
        }
        else
            registration(etEmailReg.getText().toString().trim(), etPasswordReg.getText().toString().trim());
    }
}
