package ru.ice_aqua.rundrawer.auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import ru.ice_aqua.rundrawer.ui.HelloActivity;
import ru.ice_aqua.rundrawer.R;
import ru.ice_aqua.rundrawer.ui.UserActivity;

public class LoginActivity extends AppCompatActivity{

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    ImageButton fb, google;
    Button btnAvt, btnReg, sendPassword;
    EditText etEmail, etPassword, emailForgot;
    private static int RC_SIGN_IN = 1;
    GoogleApiClient mGoogleApiClient;
    private static String TAG = "LoginActivity";
    TextView forgot;
    String email;
    AlertDialog dialog1;
    ImageView imageView;
    CallbackManager mCallbackManager;
    public ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
        Glide.with(getApplicationContext()).load("https://pbs.twimg.com/profile_images/862445411017138176/ZZV_0Xlt.jpg").placeholder(R.drawable.xxx).crossFade(1200).error(R.drawable.zzz).into(imageView);
        googleProvider();

        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Intent intent = new Intent(LoginActivity.this, HelloActivity.class);
                    startActivity(intent);
                }
            }
        };

        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            intent();
        }

        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog();

                sendPassword.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        email = emailForgot.getText().toString();
                        sendPassword(email);
                    }
                });
            }
        });

        btnAvt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etEmail.getText().toString().trim().length() > 0 && etPassword.getText().toString().trim().length() > 0) {
                    singin(etEmail.getText().toString(), etPassword.getText().toString());
                    etEmail.setText("");
                    etPassword.setText("");
                }
                else {
                    Toast.makeText(LoginActivity.this, "Пустые поля!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
            }
        });
        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               VKSdk.login(LoginActivity.this);
            }
        });
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                // Пользователь успешно авторизовался
                Toast.makeText(LoginActivity.this, "Succsess", Toast.LENGTH_SHORT).show();
                // не пойму, как войти пол-лю, чтобы как через вход гугл, создавал пол-ля в firebase
            }
            @Override
            public void onError(VKError error) {
                // Произошла ошибка авторизации (например, пользователь запретил авторизацию)
                Toast.makeText(LoginActivity.this, "Error", Toast.LENGTH_SHORT).show();

            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);

        }

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);

            } else {
                // Google Sign In failed, update UI appropriately
                // ...
            }
        }
      //  mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        showProgressDialog();
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            intent();
                            //          FirebaseUser user = mAuth.getCurrentUser();
                            //          updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                          hideProgressDialog();
                        // ...
                    }
                });
    }

    private void singin(String email, String password) {
        showProgressDialog();
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    intent();
                    hideProgressDialog();
                } else
                    Toast.makeText(getApplicationContext(), R.string.nevernii_login, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void intent() {
        Toast.makeText(LoginActivity.this, R.string.login_sucsess, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(LoginActivity.this, UserActivity.class);
        startActivity(intent);
    }

    public void googleProvider() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(getApplicationContext(), "Ошибка входа", Toast.LENGTH_SHORT).show();
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    public void init() {
        btnAvt = (Button) findViewById(R.id.btn_avt);
        btnReg = (Button) findViewById(R.id.btn_reg);
        imageView = (ImageView) findViewById(R.id.start_iv);
        etEmail = (EditText) findViewById(R.id.et_email);
        etPassword = (EditText) findViewById(R.id.et_password);
        fb = (ImageButton) findViewById(R.id.fb);
        google = (ImageButton) findViewById(R.id.google);
        forgot = (TextView) findViewById(R.id.forgot);
        mCallbackManager = CallbackManager.Factory.create();
    }

    public void sendPassword(String email) {
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), "Пароль успешно отправлен на указанный адрес", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Повторите попытку", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        dialog1.dismiss();
    }

    public void alertDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(LoginActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.forgot_password, null);
        emailForgot = (EditText) mView.findViewById(R.id.email_forgot);
        sendPassword = (Button) mView.findViewById(R.id.send_password);
        dialog.setView(mView);
        dialog1 = dialog.create();
        dialog1.show();
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}







































/*  private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    public static final int T = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        startActivityForResult(AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setProviders(
                        AuthUI.GOOGLE_PROVIDER,
                        AuthUI.FACEBOOK_PROVIDER,
                        AuthUI.EMAIL_PROVIDER)
                .build(), T);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {

    }

}*/