package ru.ice_aqua.rundrawer.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import ru.ice_aqua.rundrawer.R;
import ru.ice_aqua.rundrawer.ui.UserActivity;


public class Notific extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);

        Intent alarmIntent = new Intent(context, UserActivity.class);
        alarmIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 100, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_run_small)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.run))
                .setContentTitle("RunDrawer")
                .setContentText("Пора бежать!")
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true);

        notificationManager.notify(100, builder.build());
    }
}
