package ru.ice_aqua.rundrawer.notification;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import ru.ice_aqua.rundrawer.R;
import ru.ice_aqua.rundrawer.ui.UserActivity;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Intent intent = new Intent(this, UserActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        NotificationCompat.Builder n = new NotificationCompat.Builder(this);
        n.setContentTitle("Running");
        n.setContentText(remoteMessage.getNotification().getBody());
        n.setAutoCancel(true);
        n.setSmallIcon(R.drawable.ic_run_small);
        n.setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(),R.drawable.run));
        n.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0,n.build());
    }
}
